package com.course.cart.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CartItem {

    @Id
    @GeneratedValue
    private Long id;

    private Long productId;

    private Integer quantity;

    private String illustration;

    private String name;

    private Double price;

    public CartItem() {
    }

    public CartItem(Long id, Long productId, Integer quantity, String illustration, String name, Double price) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.illustration = illustration;
        this.name = name;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getIllustration() { return illustration; }

    public void setIllustration(String illustration) { this.illustration = illustration; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Double getPrice() { return price; }

    public void setPrice(Double price) { this.price = price; }

    @Override
    public String toString() {
        return "Cart Item :"+id+":"+productId+":"+quantity;
    }

}
