package com.course.cart.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cart {

    @Id
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<CartItem> products = new ArrayList<>();

    public Cart(Long id) {
        this.id = id;
    }

    public Cart() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<CartItem> getProducts() {
        return products;
    }

    public void addProduct(CartItem product) {
        this.products.add(product);
    }

    public CartItem getCartItemFromCart(Long id) {
        for (CartItem cartItem: products) {
            if (cartItem.getProductId() == id)
                return cartItem;
        }
        return null;
    }

}
