package com.course.cart.controllers;

import com.course.cart.domain.Cart;
import com.course.cart.domain.CartItem;
import com.course.cart.repositories.CartItemRepository;
import com.course.cart.repositories.CartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
public class CartController {

    @Autowired
    CartRepository cartRepository;

    @Autowired
    CartItemRepository cartItemRepository;

    @PostMapping(value = "/cart")
    public ResponseEntity<Cart> createNewCart()
    {
        Cart cart = cartRepository.save(new Cart(1234L));

        if (cart == null)
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Couldn't create a new cart");

        return new ResponseEntity<>(cart, HttpStatus.CREATED);
    }

    @GetMapping(value = "/cart/{id}")
    public Optional<Cart> getCart(@PathVariable Long id)
    {
        Optional<Cart> cart = cartRepository.findById(id);

        if (cart == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't get cart");

        return cart;
    }

    @PostMapping(value = "/cart/{id}")
    public ResponseEntity<CartItem> addProductToCart(@PathVariable Long id, @RequestBody CartItem cartItem)
    {
        Cart cart = cartRepository.findById(id).get();

        if (cart == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't get cart");

        CartItem cartItem1 = cart.getCartItemFromCart(cartItem.getProductId());
        if (cartItem1 == null)
            cart.addProduct(cartItem);
        else {
            cartItem.setQuantity(cartItem.getQuantity() + cartItem1.getQuantity());
            cart.getProducts().set(cart.getProducts().indexOf(cartItem1), cartItem);
        }

        cartRepository.save(cart);

        return new ResponseEntity<CartItem>(cartItem, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/cart")
    public void emptyCart()
    {
        cartRepository.deleteAll();
    }

    @DeleteMapping(value = "/cart/{id}")
    public Cart deleteProductFromCart(@PathVariable Long id)
    {
        Cart cart = cartRepository.findById(1234L).get();

        Cart newCart = new Cart(1234L);

        for (CartItem cartItem: cart.getProducts()) {
            if (cartItem.getId() != id)
                newCart.addProduct(cartItem);
        }

        cartRepository.save(newCart);

        return newCart;
    }

}
