package com.course.client.beans;

public class CartItemBean {

    private Long id;

    private Long productId;

    private Integer quantity;

    private String illustration;

    private Double price;

    private String name;

    public CartItemBean() {
    }

    public CartItemBean(Long id, Long productId, Integer quantity, String illustration, String name, Double price) {
        this.id = id;
        this.productId = productId;
        this.quantity = quantity;
        this.illustration = illustration;
        this.price = price;
        this.name = name;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getIllustration() { return illustration; }

    public void setIllustration(String illustration) { this.illustration = illustration; }

    public Double getPrice() { return price; }

    public void setPrice(Double price) { this.price = price; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    @Override
    public String toString() {
        return "Cart Item :"+id+":"+productId+":"+quantity;
    }

}
