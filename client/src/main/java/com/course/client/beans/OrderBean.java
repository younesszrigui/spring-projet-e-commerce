package com.course.client.beans;

import java.util.ArrayList;
import java.util.List;

public class OrderBean {

    private Long id;

    private List<OrderItemBean> products = new ArrayList<>();

    private Double total;

    public OrderBean() {
    }

    public OrderBean(Long id, Double total) {
        this.id = id;
        this.total = total;
    }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Double getTotal() { return total; }

    public void setTotal(Double total) { this.total = total; }

    public List<OrderItemBean> getProducts() {
        return products;
    }

    public void addOrderItem(OrderItemBean product) {
        this.products.add(product);
    }
}
