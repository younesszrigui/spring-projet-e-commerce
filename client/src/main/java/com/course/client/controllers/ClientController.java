package com.course.client.controllers;

import com.course.client.beans.*;
import com.course.client.proxies.MsCartProxy;
import com.course.client.proxies.MsOrderProxy;
import com.course.client.proxies.MsProductProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
public class ClientController {

    @Autowired
    private MsProductProxy msProductProxy;

    @Autowired
    private MsCartProxy msCartProxy;

    @Autowired
    private MsOrderProxy msOrderProxy;

    private Long cartId = 1234L;

    @RequestMapping("/")
    public String index(Model model) {

        List<ProductBean> products =  msProductProxy.list();

        model.addAttribute("products", products);

        return "index";
    }

    @RequestMapping("/product/{id}")
    public String productDetails(Model model, @PathVariable Long id) {

        Optional<ProductBean> product = msProductProxy.get(id);

        model.addAttribute("product", product.get());

        return "product";
    }

    @RequestMapping("/cart")
    public String cartDetails(Model model){

        CartBean cart = msCartProxy.getCart(cartId).orElse(null);

        double montant = 0.0;

        if (cart != null && cart.getProducts().size() > 0){
            for (int i = 0; i < cart.getProducts().size(); i++) {
                long ProductId = cart.getProducts().get(i).getProductId();
                Optional<ProductBean> productBean = msProductProxy.get(ProductId);
                montant += productBean.get().getPrice() * cart.getProducts().get(i).getQuantity();
            }

            model.addAttribute("cartItems", cart.getProducts());
        }

        model.addAttribute("totalPrice", montant);
        return "cart";
    }

    @PostMapping("/addToCart/{productId}")
    public String addProductToCart(Model model, @PathVariable Long productId, @RequestParam("quantity") int quantity) {

        if (!msCartProxy.getCart(cartId).isPresent())
            msCartProxy.createNewCart();

        ProductBean product = msProductProxy.get(productId).get();

        CartItemBean cartItemBean = new CartItemBean();
        cartItemBean.setProductId(productId);
        cartItemBean.setQuantity(quantity);
        cartItemBean.setIllustration(product.getIllustration());
        cartItemBean.setName(product.getName());
        cartItemBean.setPrice(product.getPrice());

        msCartProxy.addProductToCart(cartId, cartItemBean);
        Optional<CartBean> cartBean = msCartProxy.getCart(cartId);

        model.addAttribute("cart", cartBean.get());
        model.addAttribute("products", cartBean.get().getProducts());


        return "redirect:/cart";
    }


    @GetMapping("/order")
    public String order() {

        return "order";
    }

    @PostMapping("/order")
    public String addCartToOrder(Model model) {

        OrderBean orderBean = new OrderBean();

        CartBean cartBean = msCartProxy.getCart(1234L).get();

        for (CartItemBean cartItemBean: cartBean.getProducts()) {

            OrderItemBean orderItem = new OrderItemBean();
            orderItem.setPrice(cartItemBean.getPrice());
            orderItem.setProductId(cartItemBean.getProductId());
            orderItem.setQuantity(cartItemBean.getQuantity());
            orderItem.setName(cartItemBean.getName());
            orderBean.addOrderItem(orderItem);
        }

        orderBean.setTotal(100.0);

        msOrderProxy.addOrder(orderBean);

        msCartProxy.emptyCart();

        return "redirect:/order";
    }

    @RequestMapping("/orderList")
    public String orderList(Model model) {

        List<OrderBean> orders =  msOrderProxy.getAllOrder();

        model.addAttribute("orders", orders);

        return "orderList";
    }

    @PostMapping("/deleteCartItem/{id}")
    public String deleteItemFromCart(Model model, @PathVariable Long id) {

        CartBean cartBean = msCartProxy.deleteProductFromCart(id);

        return "redirect:/cart";
    }

}
