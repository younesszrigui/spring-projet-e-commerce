package com.course.order.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Orders {

    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(cascade = CascadeType.ALL)
    private List<OrderItem> products;

    private Double total;

    public Orders(Long id, double total) {
        this.id = id;
        this.total = total;
    }

    public Orders() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getTotal() { return total; }

    public void setTotal(double total) { this.total = total; }

    public List<OrderItem> getProducts() {
        return products;
    }

    public void addOrderItem(OrderItem product) {
        this.products.add(product);
    }

}
