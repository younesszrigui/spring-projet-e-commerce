package com.course.order.controllers;

import com.course.order.domain.Orders;
import com.course.order.repositories.OrderItemRepository;
import com.course.order.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RestController
public class OrderController {

    @Autowired
    OrderRepository orderRespository;

    @Autowired
    OrderItemRepository orderItemRepository;

    @GetMapping(value = "/orders")
    public List<Orders> getAllOrder() {

        List<Orders> order = orderRespository.findAll();

        return order;
    }

    @PostMapping(value = "/order")
    @Transactional
    public ResponseEntity<Orders> addOrder(@RequestBody Orders order){

        orderRespository.save(order);

        return new ResponseEntity<Orders>(order, HttpStatus.CREATED);
    }

}
