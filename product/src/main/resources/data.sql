INSERT INTO PRODUCT(id, name , description, illustration, price) values
(0, 'Apple AirPods Pro', 'À propos de cet article', 'https://m.media-amazon.com/images/I/71zny7BTRlL._AC_UY327_FMwebp_QL65_.jpg', 180),
(1, 'Apple iPhone 12 Pro Max (128 Go)', 'À propos de cet article', 'https://m.media-amazon.com/images/I/71CaXdX4AJL._AC_UY327_FMwebp_QL65_.jpg', 1259),
(2, 'Toshiba Disque dur externe', 'À propos de cet article', 'https://m.media-amazon.com/images/I/910A6B1Sa4L._AC_UY327_FMwebp_QL65_.jpg', 65.65),
(3, 'Apple Watch Series 6', 'À propos de cet article', 'https://images-na.ssl-images-amazon.com/images/I/717583vN3IL._AC_SL1500_.jpg', 459),
(4, 'Beats Studio3 Casque', 'À propos de cet article', 'https://images-na.ssl-images-amazon.com/images/I/51bgDQ0vGVL._AC_SL1000_.jpg', 250),
(5, 'Enceinte connectée', 'À propos de cet article', 'https://images-na.ssl-images-amazon.com/images/I/61yoaHNu9HL._AC_SL1000_.jpg', 60),
(6, 'Support Ordinateur Portable', 'À propos de cet article', 'https://images-na.ssl-images-amazon.com/images/I/71LdR4A8TRL._AC_SL1500_.jpg', 25),
(7, 'Caméra Surveillance WiFi', 'À propos de cet article', 'https://images-na.ssl-images-amazon.com/images/I/714d9sV8PZL._AC_SL1500_.jpg', 29.99),
(8, 'Câble HDMI 4K 2m', 'À propos de cet article', 'https://images-na.ssl-images-amazon.com/images/I/61FZeqrnl3L._SL1001_.jpg', 9.99);